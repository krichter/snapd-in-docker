#!/usr/bin/python

import subprocess as sp
import time
import plac


@plac.annotations(docker=("The docker command to use", "option"),
        container_name=("The container name to use", "option"),
        cmds=("The command which is executed in the snapd container (e.g. 'python -' allows you to send a Python script into the container to be executed there", 'positional', None, str, None))
def run(docker="docker", container_name="snapd-in-docker", *cmds):
    try:
        start_container(docker, container_name)
        sp.call([docker, "exec", "-i", container_name] + list(cmds))
    finally:
        sp.check_call([docker, "stop", container_name])


def start_container(docker, container_name, image_name="krichter/snapd-in-docker:centos-7"):
    sp.check_call([docker, "pull", image_name])
    sp.call([docker, "run", "--name", container_name, "--rm", "-d", "--privileged=true", "--dns", "8.8.8.8", "--tmpfs", "/run", "--tmpfs", "/tmp", "-v", "/sys/fs/cgroup:/sys/fs/cgroup:ro", "--cap-add", "SYS_ADMIN", "--cap-add", "CAP_MKNOD", image_name, "/sbin/init"], stdout=sp.PIPE, stderr=sp.PIPE)
    wait_secs = 5
    print("waiting %d seconds for the container to be available" % (wait_secs,))
    time.sleep(wait_secs)


if __name__ == "__main__":
    plac.call(run)
