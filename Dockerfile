FROM docker:dind
ENV PATH /snap/bin:/var/lib/snapd/snap/bin:$PATH
RUN apk add python3 bash
RUN pip3 install plac
ADD snapd_in_docker.py .
CMD bash
