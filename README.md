# snapd in Docker
This project consists of a Dockerfile and a script to use it providing a way to start a shell with snapd running in Docker.

The container is based on CentOS 7 because I didn't manage to start other versions and OS. That doesn't mean it's not feasible. It has been tested on Ubuntu 19.10.

## Usage
If a container with name `snapd-in-docker` is already running you need to stop it which is normally done by the script.
