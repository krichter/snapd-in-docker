#!/usr/bin/python

import subprocess as sp


def install_and_start_microk8s(snap="snap"):
    sp.check_call([snap, "install", "microk8s", "--classic"])
    sp.check_call(["microk8s.start"])
    sp.check_call(["microk8s.status", "--wait-ready"])
    sp.check_call(["microk8s.enable", "dns", "dashboard"])


if __name__ == "__main__":
    install_and_start_microk8s()
